//
//  Stats.h
//  DropPlot
//
//  Created by Michal Cisarik on 10/12/13.
//
//

#import <Foundation/Foundation.h>

@interface Stats : NSObject {
    NSMutableArray *inputcsvs;
}
@property (retain)NSMutableArray *inputcsvs;
@end
