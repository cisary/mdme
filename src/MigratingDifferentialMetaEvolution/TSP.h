/**
 * @brief Asymmetric Traveling Salesman Problem
 * @author doc. MSc. Donald Davendra Ph.D.
 * @date 3.10.2013
 *
 * This is a simple class to calcultae the cost of a TSP tour.
 */

//using namespace std;

class cTSP{
	
public:
    //! A constructor.
    /*!
     Constructs the TSP class, and assigns the values.
     */
	cTSP();
    
    //! A destructor.
    /*!
     Clears the memory.
     */
	~cTSP();
    
    //! A normal member taking in the tour and returning the cost.
    /*!
     \param Tour A TSP tour
     \return The cost of the tours
     */
	float TourCost(int *Tour);
    
    //! Returns the number of cities in the tour.
    /*!
     \param no parameters
     \return The number fo cities in the tour
     */
    int GetCities();
    
    void TourFunction(cTSP *TSP);
	
private:
    //! The distances of the cities in the tour.
	float* m_Distance;
    //! The number of cities in the tour.
    int m_Cities;
};
