//
//  Coevolution_Tests.m
//  Coevolution Tests
//
//  Created by Michal Cisarik on 11/12/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Coevolution.h"

#include <iostream>
#include <fstream>

using namespace std;

@interface MDME_Tests : XCTestCase

@end

@implementation MDME_Tests {
    Coevolution* coevolution;
}

- (void)setUp {
    [super setUp];
    coevolution=[Coevolution newCoevolutionForProblem:TSP];
}

- (void)tearDown {
    [coevolution free];
    [super tearDown];
}

- (void)testMDMECoevolve {
    [coevolution coevolve];
}

@end
