#import <Cocoa/Cocoa.h>

@interface InputView : NSView {
    NSImage * _selected;
	NSImage * _imageForExample1;
}

- (NSString *)selected;
- (void)setSelected:(NSString *)aValue;

@end
