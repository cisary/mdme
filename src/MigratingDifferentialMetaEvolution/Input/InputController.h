//
//  InputController.h
//  IntroToQuartzPartTwo
//
//  Created by Scott Stevenson on 11/27/06.
//
//  The tutorial and code were written by Scott Stevenson:
//  Personal site: http://theocacao.com/
//  Email: sstevenson@mac.com
// 
//  The code in this project is intended to be used as a learning
//  tool for Cocoa programmers. You may freely use the code in
//  your own programs, but please do not use the code as-is in
//  other tutorials.
//
//  This code is for the "Introduction to Quartz II" tutorial at:
//  <http://cocoadevcentral.com/d/intro_to_quartz_two/>


#import <Cocoa/Cocoa.h>


@interface InputController : NSObject {

	IBOutlet NSView			* view;
	IBOutlet NSController	* arrayController;

	NSArray  * _exampleNames;	
	NSString * _selected;
}

- (NSArray *)exampleNames;
- (void)setExampleNames:(NSArray *)aValue;
- (NSString *)selected;
- (void)setSelected:(NSString *)aValue;

@end
