#import "InputController.h"

@interface InputController (Private)
- (void)configureExampleNames;
@end


@implementation InputController

- (void)awakeFromNib
{   
	
	// Build an array of example names for the dropdown
	// control in the UI. Also set an initial value.
	[self configureExampleNames];
	[self setSelectedExample:@"Example1"];
	
	// Bind InputView to the array controller.
	//
	// We bind to "description" because the examples
	// array is just a list of strings. The "description"
	// key will give us the string value itself (such as
	// "Example4").
	//
	// If we just bound directly to "selection", the view
	// would be passed a NSController proxy object and get
	// very confused. All we care about is the string value
	// itself.
		
	[view bind: @"selected"
      toObject: arrayController
   withKeyPath: @"selection.description"
       options: nil];
}


- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)app
{
	// make sure the app closes when the window closes
	return YES;
}


#pragma mark -
#pragma mark Private

- (void)configureExampleNames
{
	NSMutableArray *array = [NSMutableArray array];   
	[array addObject: @"Example1"];
	[array addObject: @"Example2"];
	[array addObject: @"Example3"];
	[array addObject: @"Example4"];
	[array addObject: @"Example5"];
	[array addObject: @"Example6"];
	[array addObject: @"Example7"];
	[self setExampleNames:array];	
}


#pragma mark -
#pragma mark Accessors

- (NSArray *)exampleNames
{
	return _exampleNames;
}

- (void)setExampleNames:(NSArray *)aValue
{
	NSArray *oldExampleNames = _exampleNames;
	_exampleNames = [aValue copy];
	[oldExampleNames release];
}

- (NSString *)selectedExample
{
	return _selected;
}

- (void)setSelectedExample:(NSString *)aValue
{
	NSString *oldSelected = _selected;
	_selected = [aValue copy];
	[oldSelected release];
}

@end
