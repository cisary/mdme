//
//  Coevolution.m
//  MDME
//
//  Created by Michal Cisarik on 11/12/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import "Coevolution.h"
#import "MDME.h"

#include <iostream>
#include <fstream>

using namespace std;

ofstream logg("../../output/log.txt");


@implementation Coevolution {
    // Variables, which are usable inside block
    // "dispatch_group_async(evolutions,high,^{ ... });"
    // where MDME algorithms take place.
    // race conditions are solved by global high-priority OSX dispatcher
    
    // This is also preparation for OpenMP/MPI cluster port
    __block NSMutableArray* evolutions;
    __block Problem problem;
    __block Optimization optimization;
    __block MDME* alg;
    
    //Problem problem;
    //Optimization optimization;
    
    // Variables for grand central dispatch:
    dispatch_group_t evolution_threads;
    dispatch_queue_t high;
    dispatch_queue_t queue;
    
    // Migrating vectors matrix:
    int **migrating;
    
    // other variables which blocks need (configuration):
    __block NSNumber *vect;
    __block NSNumber *generations;
    __block NSNumber *scalingFactor;
    __block NSNumber *crossProb;
    __block NSNumber *mutProb;
    __block NSNumber *migProb;
    __block NSNumber *migrations;
    
    // just indexes used within the block
    __block int i,ib;
    
    // loop indexes and 'constants'
    int j,k,threads,gen;
    
    // stats:
    double current;
    double maximum;
    double minimum;
    double average;
    
    // string buffer:
    NSString *s;
}

// Constructor
+ (id) newCoevolutionForProblem:(Problem)p  {
    Optimization o;
    switch (p) {
        case TSP:
            o=MINIMUM;
            break;
        case FSS:
            o=MINIMUM;
            break;
        default:
            break;
    }
    return [[Coevolution alloc] initWithProblem:p toFind:o];
}

// Instantiation
- (id)initWithProblem:(Problem)p toFind:(Optimization)o {
    self = [super init];
    if (self) {
        
        evolutions=[[NSMutableArray alloc]init];
        problem = p;
        optimization = o;
        
        // load all needed configuration from Configuration class:
        vect = [[Configuration all] objectForKey:@"MDME_vectors"];
        generations = [[Configuration all] objectForKey:@"MDME_generations"];
        scalingFactor = [[Configuration all] objectForKey:@"MDME_scalingFactor"];
        crossProb = [[Configuration all] objectForKey:@"MDME_crossProb"];
        mutProb = [[Configuration all] objectForKey:@"MDME_mutProb"];
        migProb = [[Configuration all] objectForKey:@"MDME_migProb"];
        migrations = [[Configuration all] objectForKey:@"MDME_migrations"];
        
        // create grand central dispatch queue:
        queue = dispatch_queue_create("com.cisary.queue",0);
        
        // obtain global queue:
        high = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,NULL);
        
        // apply high-priority:
        dispatch_set_target_queue(queue,high);
        
#ifdef DEBUG
        logg << "Coevolution queue 'com.cisary.queue' created and configured to high priority. Target queue set.";
        logg.flush();
#endif
        
        // with grand central dispatch set we can create our own block 'group':
        evolution_threads = dispatch_group_create();
        
#ifdef DEBUG
        logg << " Evolutions thread group created." << endl;
        logg.flush();
#endif
        
        // allocate matrix for vector migration:
        migrating = (int **) malloc(sizeof(int*) * [migrations intValue]);
        
        if (problem==TSP)
            migrating[0] = (int *) malloc(sizeof(int) * 70 * [migrations intValue]);
        else if (problem==FSS)
            migrating[0] = (int *) malloc(sizeof(int) * 5 * [migrations intValue]);
        // 70 in here is fixed - TODO parameters from problems input loading..
        
        // reconfigure migrating matrix for correct indexing:
        for ( i = 0; i < [migrations intValue]; i++ )
			if(i)
                migrating[i] = migrating[i-1] + [migrations intValue];
#ifdef DEBUG
        logg << " Memory space for migrating vectors created." << endl;
        logg.flush();
#endif
    }
    return self;
}


-(void)coevolve {
    
    id best;
    
    threads = [migrations intValue];
    
    if (optimization == MINIMUM)
        current = DBL_MAX;
    
    else if(optimization == MAXIMUM)
        current = 0;
    
    int i_bs,i_ga,i_de,i_mdme,i_soma; // sum of this permutation must be 'threads'

    for (j = 0; j < threads; j++) {
        alg = [[MDME alloc] initWithVectors:vect
                                      generations:generations
                                    scalingFactor:scalingFactor
                                        crossProb:crossProb
                                          mutProb:mutProb
                                          migProb:migProb
                                       migrations:migrations
                                   defaultProblem:problem
                                             find:optimization
                                             seed:j];
        
        [evolutions addObject:alg];
    }
    
    for (gen = 0; gen < [generations intValue]; gen++) {
    
#ifdef DEBUG
        logg << endl << gen << ". generation of convolution starting.." << endl;
        logg.flush();
#endif
        
        i=0;
        // w=0;
        
        ib=0;
        
        __block NSMutableDictionary *indexes = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@0,@"i",@0,@"ia",@0,@"ib",nil] ;
        
        
        for (j = 0; j < threads; j++)
            dispatch_group_async(evolution_threads, high, ^{ // parallel BLOCK starts here...
                
#ifdef DEBUG
                logg << "thread " << [[indexes objectForKey:@"i"]intValue] << "starting metaevolution.." << endl;
                s = [NSString stringWithFormat:@"thread %i starting metaevolution..\n",[[indexes objectForKey:@"i"]intValue]];
                //NSLog(@"%@",s);
                logg.flush();
#endif
                
                // local variables of the block (thread-specific):
                NSString *s;
                MDME *a;
                
                @try {
                    @synchronized (indexes) {
                        
                        int index=[[indexes objectForKey:@"i"]intValue];
                        
                        a=[evolutions objectAtIndex: index ];
                        
                        [indexes
                         setValue:[NSNumber numberWithInt:index+1]
                           forKey:@"i"];
                    }
                    
                    [a evolve]; // now, when we have MDME object we can proceed metaevolution itself
                    
#ifdef DEBUG
                    logg << "...thread " << [[indexes objectForKey:@"i"]intValue] << " is done." << endl;
                    s = [NSString stringWithFormat:@"...thread %i is done.\n",[[indexes objectForKey:@"i"]intValue]];
                    //NSLog(@"%@",s);
                    logg.flush();
#endif
                }
                
                // if anything went wrong catch exception, log it and do what is needed to continue evaluation of coevolution
                @catch(NSException *e) {
                    
                    s = [NSString stringWithFormat:@"\n!\n!\n!Exception: %@",[e name]];
                    
                    NSLog(@"%@",s);
                    
                    logg << endl << [s UTF8String] << endl;
                    logg.flush();
                    
                    if ([[e name] isEqualToString:@"generationswithnochange reached!"]){
                    }
                        //[alg resetMigrate];
                    
                    else if ([[e name] isEqualToString:@"newest extreme ever found!"]) {
                        
                        //@throw e; ?? What should I do with exception in BLOCK?
                    }
                }
            });
        
        dispatch_group_wait(evolution_threads, INFINITY); // barrier !
        
        s=[NSString stringWithString:@""];
        
        int x=0;
        
        if (problem==FSS)
            x=5;
        else if (problem==TSP)
            x=70;
        
        average = 0;
        i=0;
        
        for (MDME* alg in evolutions) {
            for(j=0;j<x;j++)
                migrating[i][j]=[alg bestbuffer][j];
            i++;
            average+=[alg bestFitness];
        }
        
        average = average/(double)i;
        i=0;
        
        for (MDME* alg in evolutions) {
            
#ifdef DEBUG
            s=[NSString stringWithFormat:@"MDME%i Best:\n%@\n", i, alg];
            logg << [s UTF8String] << endl;
            NSLog(@"%@",s);
            logg.flush();
#endif
            
            if (((optimization == MAXIMUM)&&([alg bestFitness] > current)) ||
             ((optimization == MINIMUM)&&([alg bestFitness] < current))) {
                current = [alg bestFitness];
                best = alg;
            }
            i++;
        }
        
#ifdef DEBUG
        s=[NSString stringWithFormat:@"%@\n", best];
    NSLog(@"------------------------------------------------------------------------");
        logg << "------------------------------------------------------------------------" << endl << gen << ". generation of coevolution\nbest :" << endl << [s UTF8String];
        NSLog(@"%i. generation of coevolution\nbest :\n %@\n",gen,best);
        NSLog(@"Coevolution average : %.0f",average);
    NSLog(@"------------------------------------------------------------------------");
        logg << "Coevolution average : " << average << endl;
        logg << "------------------------------------------------------------------------" << endl;
        logg.flush();
#endif
        
        for (MDME* alg in evolutions) {
            
            [alg migrate:migrating];
        
        }
            
    }
    logg.close();
}



-(void)free{
    for (id e in evolutions)
        [e free];
    [evolutions removeAllObjects];
    evolutions=nil;
    free(migrating[0]);
    free(migrating);
}
@end
