//
//  Coevolution.h
//  MDME
//
//  Created by Michal Cisarik on 11/12/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "types.h"

//! C includes:
//#include "mgl_cf.h" // MathGL

// CPU process dispatching class for multiple evolutions - coevolution with migration
@interface Coevolution : NSObject
+(id)newCoevolutionForProblem:(Problem)p;
-(id)initWithProblem:(Problem)p toFind:(Optimization)o;
-(id)bestEvolution;
-(void)coevolve;
-(void)free;
@end
