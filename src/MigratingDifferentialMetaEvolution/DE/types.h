
typedef enum {
    TSP,
    FSS
} Problem;

typedef enum {
    MINIMUM,
    MAXIMUM
} Optimization;