//
//  Configuration.h
//  MDME
//
//  Created by Michal Cisarik on 11/12/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Configuration : NSObject {
    
}
+(NSDictionary*) all;
@end
