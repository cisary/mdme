//
//  PADME.h
//  PADME
//
//  Created by Michal Cisarik on 12/19/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSP.h"

@interface PADME : NSObject
-(BOOL)_isinarray:(int*)array len:(int)length element:(int)element;
-(int)evolve;
-(void)de:(float**)vectors;
-(void)initArray:(float**)array;
-(float)fitness:(int*)route;
-(void)findMinimum;
-(void)free;
-(void)transform;
-(void)adapt;
-(void)run;
@end
