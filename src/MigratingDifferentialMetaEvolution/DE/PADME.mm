//
//  PADME.mm
//  PADME - Permutative adapting differential meta evolution
//  Main idea is that valid permutations should be generated just once.
//  Instead of repairing (transforming) individual vectors to permutations before each evaluation and then generating repairing (transforming) array by meta evolution (as it is implemeneted in MDME)
//  In (Permutative) Adaptive Differenatial Meta Evolution - PADME metaevolution (by DE) is used just to evolve transforming (de0_sub) arrays and create subpermutation as (de1_shuffle) array metaevolves. Transformation itself is done by creating binary three of subpermutations (de0_sub) with topology based on shuffle (de1_shuffle) metaevolving array. By traversing newly created tree (in Order) it is clear that result is valid permutation (because every subpermutation is valid) which can be easily evaluated without repairing.
//  In this commit there is few "experimentals elements" like Adaptation (parametrizaters metaevolution as DE is running). which are not yet tested and probably wont be in final release anyway.
//
//  Created by Michal Cisarik on 12/19/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import "PADME.h"
#import "BinaryTree.h"
#import "Node.h"
/*
 #include <iostream>
 #include <fstream>
 #include <algorithm>
 #include <iostream>
 #include <vector>
 #include <cstdlib>
 #include <ctime>
 */
#include <random>

using namespace std;

// thread-safe C++ mersenne twister:
typedef std::mt19937_64 MT;

// Thread-safe mersenne twister declaration
uniform_int_distribution<MT::result_type> mtint(0, INTMAX_MAX);
uniform_real_distribution<double> mtfloat0_1(0.0, 1.0);

@implementation PADME {
    
    MT mt;
    
    cTSP *tsp;
    
    int v,w,imeta,a,i,j,jj,k,kk,l,ll,m,N,X,Y,Z,dimX,dimY,dimension,vector,r1,r2,r3,Xu,Xl,repairindex,x,y,fitness,fittest,max;
    
    BOOL direction;
    
    float trialfitness,bestfitness,beforefitness,newfitness;
    int improvements;
    
    // matrix arrays pointers for malloc
    int **valid_permutations;
    float **adaptation;
    float **vectors;
    float **de0_subpermutations_sums;
	float **de1_subpermutations_shuffles;
    
    float *trial;
    float *trialbuffer;
    float *de0buffer;
    float *de1buffer;
    int *bestvalidbuffer;
    int *transformingbuffer;
    int *bestshufflebuffer;
    int *bestsubsbuffer;
    int *parameters;
    float *valid_permutation;
    
    // properties of DE itself
    float scalingFactor,crossProb,mutProb;
    
    // stringbuffers:
    NSMutableString *buffer;
    NSMutableString *mutableout;
    
    int first,firstSub,firstDirection,subs,sub,dir;
}

static const NSInteger PARAMETERS = 4;
static const NSInteger ADAPTATION = 10;
static const NSInteger ELITISM = 10;
static const NSInteger ITERATIONS = 10;

-(void)initArray:(float**)array {
    
    for (i=0;i<dimY;i++) {
        
        for (j=0; j<dimX; j++) {
            
            array[i][j]=(mtfloat0_1(mt) * Xu);
            
            while ((array[i][j] < Xl) || (array[i][j] > Xu)) {
                if (array[i][j] < Xl)
                    array[i][j] = 2 * Xl - array[i][j];
                if (array[i][j] > Xu)
                    array[i][j] = 2 * Xu - array[i][j];
            }
            
        }
        
    }
}

- (id)init {
    self = [super init];
    if (self) {
        scalingFactor=0.9;
        crossProb=0.9;
        mutProb=0.3;
        
        mt.seed((unsigned int)time(NULL));
        
        tsp = new cTSP();
        dimension = tsp->GetCities()-1;
        X=dimension;
        
        N=dimension;
        Y=6;
        Z=8;
        
        trial = (float *)malloc(sizeof(float) * X);
        trialbuffer = (float *)malloc(sizeof(float) * X);
        de0buffer = (float *)malloc(sizeof(float) * X);
        de1buffer = (float *)malloc(sizeof(float) * X);
        transformingbuffer = (int *)malloc(sizeof(int) * X);
        
        parameters = (int *)malloc(sizeof(int) * PARAMETERS);
        
        adaptation = (float **) malloc(sizeof(float*) * ADAPTATION);
		adaptation[0] = (float *) malloc(sizeof(float) * ADAPTATION * PARAMETERS);
        
        vectors = (float **) malloc(sizeof(float*) * N);
		vectors[0] = (float *) malloc(sizeof(float) * N * X);
        
        valid_permutation = (float *)malloc(sizeof(float) * X);
        bestvalidbuffer = (int *)malloc(sizeof(int) * X);
        
        valid_permutations = (int **) malloc(sizeof(int*) * N);
		valid_permutations[0] = (int *) malloc(sizeof(int) * N * X);
        
        de0_subpermutations_sums = (float **) malloc(sizeof(float*) * N);
		de0_subpermutations_sums[0] = (float *) malloc(sizeof(float) * N * X);
        
        bestsubsbuffer = (int *)malloc(sizeof(int) * X);
        
        de1_subpermutations_shuffles = (float **) malloc(sizeof(float*) * N);
        de1_subpermutations_shuffles[0] = (float *) malloc(sizeof(float) * N * X);
        
        bestshufflebuffer = (int *)malloc(sizeof(int) * X);
        
        for(i=0;i<ADAPTATION;i++)
            if (i) {
                adaptation[i]=adaptation[i-1]+PARAMETERS;
            }
        
        for ( i = 0; i <= N; i++ ) {
			if(i) {
                vectors[i] = vectors[i-1] + X;
                valid_permutations[i] = valid_permutations[i-1] + X;
                de0_subpermutations_sums[i] = de0_subpermutations_sums[i-1] + X;
                de1_subpermutations_shuffles[i] = de1_subpermutations_shuffles[i-1] + X;
			}
		}
        
        dimY=ADAPTATION;
        dimX=PARAMETERS;
        dimension=X;
        Xu=ADAPTATION/PARAMETERS;
        Xl=1;
        
        [self initArray:adaptation];
        
        dimY=N;
        dimX=X;
        dimension=X;
        Xu=X;
        Xl=1;
        
        [self initArray:vectors];
        
        for ( v = 0; v < N; v++ ) {
            valid_permutations[v][0]=(int)vectors[v][0];
            
            for (imeta = 1; imeta < X; imeta++) {
                
                a = (int)vectors[v][imeta];
                
                valid_permutations[v][imeta] = (int)vectors[v][imeta];
                
                direction=((mtint(mt) % 2)==0);
                
                while ([self _isinarray:valid_permutations[v] len:imeta element:a]) {
                    
                    if (direction)
                        a += 1;
                    else
                        a -= 1;
                    
                    if (a > dimension)
                        a = 0;
                    
                    else if (a <= 0)
                        a = dimension;
                    
                }
                
                valid_permutations[v][imeta] = a;
            }
        }
        
        dimY=X;
        dimX=X;
        dimension=X;
        Xu=X/2;
        Xl=1;
        
        [self initArray:de0_subpermutations_sums];
        
        dimY=Z;
        dimX=X;
        dimension=X;
        Xu=1;
        Xl=-1;
        
        [self initArray:de1_subpermutations_shuffles];
        
    }
    return self;
}

-(BOOL)_isinarray:(int*)array len:(int)length element:(int)element{
    //if (a==0)
    //    return YES;
    
    BOOL is = NO;
    
    for (int i=0; i<length; i++)
        if (array[i] == element) {
            is = YES;
            break;
        }
    return is;
}

-(Node*)addNumber:(int)num asNode:(Node*)node onTheRight:(BOOL)right {
    Node *n;
    if (right)
        n = [[Node alloc] initWithData:num andLeft:nil andRight:node];
    else
        n = [[Node alloc] initWithData:num andLeft:node andRight:nil];
    return n;
}

-(void)transform {
    
    first = valid_permutations[v][0];
    firstSub=(int)de0_subpermutations_sums[v][0];// how much of numbers
    firstDirection=(int)de1_subpermutations_shuffles[w][0]; //direction of the first subpermutation
    
    Node *node=[[Node alloc] initWithData:first andLeft:nil andRight:nil];
    
    subs=0;
    sub=0;
    dir=0;
    
    for (jj=1; jj<X;jj++) {
        sub=(int)de0_subpermutations_sums[v][subs];// how much of numbers
        dir=(int)de1_subpermutations_shuffles[w][sub];
        
        for (kk=0; kk<sub; kk++)
            
            node=[self addNumber:valid_permutations[vector][jj] asNode:node onTheRight:(dir==0)];
        
        subs++;
    }
    
    BinaryTree *bt = [[BinaryTree alloc] initWithRoot:node];
    
    __block int index=0;
    
    [bt traverse:^int(Node* n){
        transformingbuffer[index++]=[n data]; // save newly shuffled node as result
        //n=nil;???
        return 0;
    }];
    
    bt=nil;//??
    
}

-(void)run {
    
    fittest=FLT_MAX;
    
    for (ll=0; ll<100; ll++)
        [self adapt];
    
}

-(void)adapt {
    
    
    for(a=0;a<ADAPTATION;a++) {
        dimension=PARAMETERS;
        Xu=ADAPTATION/PARAMETERS;
        Xl=1;
        v=a;
        [self de:adaptation];
        adaptation[a][0]=trial[0];
        adaptation[a][1]=trial[1];
        fitness=[self evolve];
        
        if (fitness<fittest) {
            fittest=fitness;
            max=a;
        }
    }
    a=fittest;
    for (l=0; l<ELITISM; l++)
        [self evolve];
    
}

-(int)evolve {
    improvements=0;
    for(int e=0;e<ITERATIONS;e++) {
        
        for (vector=0;vector<=N;vector++) {
            
            beforefitness=[self fitness:&valid_permutations[vector][0]];
            
            for (v=0; v<1; v++) {//(int)adaptation[a][0]
                Xu=X/2;
                Xl=1;
                
                [self transform];
                fitness=[self fitness:transformingbuffer];
                
                [self de:de0_subpermutations_sums];
                
                for (k=0; k<X; k++) {
                    de0buffer[k]=de0_subpermutations_sums[vector][k];
                    de0_subpermutations_sums[vector][k]=trial[k];
                }
                
                for (w=0; w<1; w++) {//(int)adaptation[a][0]
                    
                    [self transform];
                    newfitness=[self fitness:transformingbuffer];
                    
                    Xu=1;
                    Xl=-1;
                    [self de:de1_subpermutations_shuffles];
                    
                    for (k=0; k<X; k++) {
                        de1buffer[k]=de1_subpermutations_shuffles[w][k];
                        de1_subpermutations_shuffles[w][k]=trial[k];
                    }
                    
                    [self transform];
                    
                    trialfitness=[self fitness:transformingbuffer];
                    if (trialfitness<newfitness) {
                        newfitness=trialfitness;
                        improvements++;
                    }
                    
                        for (k=0; k<X; k++)
                            de1_subpermutations_shuffles[w][k]=de1buffer[k];
                        
                        [self transform];
                    
                    
                    if (trialfitness<bestfitness){
                        bestfitness=trialfitness;
                        
                        buffer=[NSMutableString stringWithFormat:@""];
                        
                        for (jj=0; jj<X; jj++) {
                            bestvalidbuffer[jj]=transformingbuffer[jj];
                            valid_permutations[vector][jj]=transformingbuffer[jj];
                            [buffer appendString:[NSString stringWithFormat:@"%i,",transformingbuffer[jj]]];
                        }
                        NSLog(@"NEW BEST :{%@} Fitness=%.1f",buffer,bestfitness);
                    }
                }
                
                if (newfitness>=beforefitness) {
                    for (k=0; k<X; k++)
                        de0_subpermutations_sums[v][k]=de0buffer[k];
                    
                    //[self transform];
                }
            }
        }
    }
    return improvements;
}

//! Evolve by differential evolution
/*!
 ...
 */
-(void)de:(float**)w {
        
    // find random r1,r2,r3 in [0,numberVectors) such that r1 != j !r2 != r3 :
    while (true) {
        r1=(int)(mtint(mt) % dimension);
        if (r1!=j)
            break;
    }
    
    while (true) {
        r2=(int)(mtint(mt) % dimension);
        if ((r2!=r1) && (r2!=j))
            break;
    }
    
    while (true) {
        r3=(int)(mtint(mt) % dimension);
        if ((r3!=r2) && (r3!=r1) && (r3!=j))
            break;
    }
    
    for (k = 0; k < dimension; k++)
        trial[k] = w[r3][k] + scalingFactor * ((w[r1])[k]-(w[r2])[k]);
    
    // crossover:
    
    int n = (int)(mtint(mt) % dimension);
    int l = 0;
    
    // the element - [of crossProb || dimension )
    // TODO
    while (true) {
        
        l += 1;
        if ((((mtint(mt) % 2)==0) > crossProb) || (l > dimension))
            break;
    }
    
    for (k = 0; k < dimension; k++)
        for (kk = n; ((kk > n) && (kk < (n+l))); kk++)
            
            if (k != (kk % dimension))
                trial[k] = w[v][k];
    
    // boundaries check:
    for (k = 0; k < dimension; k++)
        while ((trial[k] < Xl) || (trial[k] > Xu)) {
            if (trial[k] < Xl)
                trial[k] = 2 * Xl - trial[k];
            
            if (trial[k] > Xu)
                trial[k] = 2 * Xu - trial[k];
        }
    
}

-(NSString *) description  {
    
    mutableout=[NSMutableString stringWithFormat:@""];
    int x;
    buffer=[NSMutableString stringWithFormat:@""];
    
    for (x = 0; x < X; x++)
        [buffer appendString:[NSString stringWithFormat:@"%.0f,",vectors[vector][x]]];
    
    [mutableout appendString:[NSString stringWithFormat:@"vectors[%i] = %@\n",vector,buffer]];
    
    buffer=[NSMutableString stringWithFormat:@""];
    
    for (x = 0; x < X; x++)
        [buffer appendString:[NSString stringWithFormat:@"%i,",valid_permutations[vector][x]]];
    
    [mutableout appendString:[NSString stringWithFormat:@"valid_permutations[%i] = %@\n",vector,buffer]];
    
    buffer = [NSMutableString stringWithFormat:@""];
    
    for (x = 0; x < X; x++)
        [buffer appendString:[NSString stringWithFormat:@"%.0f,",de0_subpermutations_sums[vector][x]]];
    
    [mutableout appendString:[NSString stringWithFormat:@"de0_subpermutations_sums[%i] = %@\n",vector,buffer]];
    
    
    buffer = [NSMutableString stringWithFormat:@""];
    
    for (x = 0; x < X; x++)
        [buffer appendString:[NSString stringWithFormat:@"%.0f,",de1_subpermutations_shuffles[vector][x]]];
    
    [mutableout appendString:[NSString stringWithFormat:@"de1_subpermutations_shuffles[%i] = %@\n-------------------------------------------\n",vector,buffer]];
    
    [self transform];
    
    buffer = [NSMutableString stringWithFormat:@""];
    
    for (x = 0; x < X; x++)
        [buffer appendString:[NSString stringWithFormat:@"%i,",transformingbuffer[x]]];
    
    [mutableout appendString:[NSString stringWithFormat:@"transformed = %@ (fitness=%0.1f)\n\n",buffer,[self fitness:transformingbuffer]]];
    
    return mutableout;
}

-(float)fitness:(int*)route {
    
     buffer=[NSMutableString stringWithFormat:@""];
     
     for (int xx=0; xx<X; xx++) {
     [buffer appendString:[NSString stringWithFormat:@"%i,",route[xx]]];
     }
     
     NSLog(@"-(float)fitness:(int*)schedule={%@} Fitness=%.1f",buffer,tsp->TourCost(route));
     
    return tsp->TourCost(route);//fss->fitness(schedule);
}

-(void)free{
    free(vectors[0]);
    free(vectors);
    
    free(valid_permutations[0]);
    free(valid_permutations);
    
    free(de0_subpermutations_sums[0]);
    free(de0_subpermutations_sums);
    
    free(de1_subpermutations_shuffles[0]);
    free(de1_subpermutations_shuffles);
    
    free(transformingbuffer);
}

@end
