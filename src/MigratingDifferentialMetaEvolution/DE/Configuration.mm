//
//  Configuration.m
//  MDME
//
//  Created by Michal Cisarik on 11/12/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

/*
 --------------------------------------------------------------------------------
 dict = @{@"MDME_vectors" : @30,
 @"MDME_generations" : @100,
 @"MDME_scalingFactor" : @0.9,
 @"MDME_crossProb" : @0.9,
 @"MDME_mutProb" : @0.3,
 @"MDME_migProb" : @0.3,
 @"MDME_migrations" : @30
 };
 
2013-11-21 21:36:15.653 xctest[12283:303] 99. generation best:
62,58,39,6,21,18,25,28,63,61,9,59,16,48,60,19,26,49,38,36,44,20,47,46,45,54,5,2,42,41,40,52,17,15,14,30,3,11,37,50,8,22,34,35,12,13,55,10,29,43,33,51,7,23,24,4,27,56,32,53,57,1,31,70,69,68,64,65,66,67,
Fitness = 57516

Test Case '-[MDME_Tests testMDMECoevolve]' passed (321.090 seconds).
Test Suite 'MDME_Tests' finished at 2013-11-21 20:36:15 +0000.
Executed 1 test, with 0 failures (0 unexpected) in 321.090 (321.090) seconds
Test Suite 'MDMETests.xctest' finished at 2013-11-21 20:36:15 +0000.
Executed 1 test, with 0 failures (0 unexpected) in 321.090 (321.090) seconds
Test Suite 'All tests' finished at 2013-11-21 20:36:15 +0000.
Executed 1 test, with 0 failures (0 unexpected) in 321.090 (321.092) seconds
 
 --------------------------------------------------------------------------------
 
*/
#import "Configuration.h"

@implementation Configuration

+(NSDictionary*) all
{
    static NSDictionary* dict = nil;
    
    if (dict == nil) {
        dict = @{@"MDME_vectors" : @20,
                 @"MDME_generations" : @500,
                 @"MDME_scalingFactor" : @0.9,
                 @"MDME_crossProb" : @0.9,
                 @"MDME_mutProb" : @0.3,
                 @"MDME_migProb" : @0.6,
                 @"MDME_migrations" : @20
                 };
    }
    
    return dict;
}

@end
