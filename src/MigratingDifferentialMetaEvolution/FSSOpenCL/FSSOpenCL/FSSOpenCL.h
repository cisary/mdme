//
//  FSSOpenCL.h
//  FSSOpenCL
//
//  Created by Michal Cisarik on 10/27/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <OpenCL/opencl.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "gpumakespan_kernel.cl.h"

#import "FSS.h"
#import "../../DE/DE.h"

@interface FSSOpenCL : NSObject {
    char name[128];
    dispatch_queue_t queue;
    cl_device_id gpu;
    
    float* in;
    float* out;
    
    
    void* mem_in;
    void* mem_out;
    
    void* d_ProcessTimeI;
    void* d_CompletionTimeI;
    int d_Jobs;
    int d_Machines;
    
    cFSS *fss;
    DE *de;
}
-(id)init;
-(float) makespans:(int**) schedules;
-(float*) calculate:(int*)o a:(double*)a b:(double*)b len:(int)len;
-(void)free;
@end

