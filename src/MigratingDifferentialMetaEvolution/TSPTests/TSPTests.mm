//
//  TSPTests.m
//  TSPTests
//
//  Created by Michal Cisarik on 10/14/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <XCTest/XCTest.h>

#include "TSP.h"

#include <iostream>
#include <fstream>

using namespace std;

ifstream in("in.txt");
ofstream out("out.txt");


@interface TSPTests : XCTestCase

@end

@implementation TSPTests

- (void)setUp
{
    [super setUp];
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    int array[70]={20,5,3,2,1,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,19,18,17,16,15,14,13,12,11,10,9,8,7,6,4};
    //Fitness = 47497

    //! Initialization of the TSP class
    cTSP* tsp = new cTSP();
    
    float result = tsp->TourCost(array);
    
    //! Calculate a simple tour
    tsp->TourFunction(tsp);
}

@end

