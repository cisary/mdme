//
//  DETests.m
//  DETests
//
//  Created by Michal Cisarik on 10/14/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

//! Objective-C classes imports:
#import <XCTest/XCTest.h>
#import "DE.h"

using namespace std;

ifstream in("in.txt");
ofstream out("out.txt");

@interface DETests : XCTestCase {
    DE* de;
    NSMutableArray *evolutions;
}
@end

@implementation DETests

- (void)setUp
{
    [super setUp];

    de = [DE newWithVectors:30 generations:10000 scalingFactor:0.9 crossProb:0.9 defaultProblem:TSP];
    
    //evolutions=[DE createMigratingMetaEvolutionsWithDimensionFSS:70 numberVectors:30 generations:1000 scalingFactor:0.9 crossProb:0.9 ];
}

- (void)tearDown
{
    [de free];
    [super tearDown];
}

- (void)testEvolveMigratingMetaEvolutions {
    //NSMutableDictionary *oneGeneration = [DE evolveMigratingMetaEvolutions:evolutions];
    //[DE freeMigratingMetaEvolutions:evolutions];
    int i=0;
}

- (void)testMetaEvolve
{
    //while (true) {
    
        @try {
            [de metaEvolve];
        }
        @catch(NSException *e) {
            
            NSLog(@"\n\n\n\Exception: %@",[e name]);
            
            if ([[e name] isEqualToString:@"generationswithnochange reached!"])
                
                [de resetMigrate];
            
            else if ([[e name] isEqualToString:@"newest extreme ever found!"]) {
                
                NSLog(@"%@",[de bestDescription]);
                //break;
            }
        }
    //}
    
    XCTAssertTrue(true);
}

@end
