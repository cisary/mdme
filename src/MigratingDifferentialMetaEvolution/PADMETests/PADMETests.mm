
//  PADMETests.m
//  PADMETests
//
//  Created by Michal Cisarik on 12/19/13.
//
//

#import <XCTest/XCTest.h>
#import "PADME.h"

@interface PADMETests : XCTestCase

@end

@implementation PADMETests {
    PADME *padme;
}

- (void)setUp
{
    [super setUp];
    padme=[[PADME alloc]init];
}

- (void)tearDown
{
    [padme free];
    [super tearDown];
}

- (void)testRun
{
    [padme run];
    
    int a=0;
}

@end
