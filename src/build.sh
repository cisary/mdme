#!/bin/sh
xcodebuild \
    -project SymbolicRegression/SymbolicRegression.xcodeproj \
    -scheme Plot \
    -sdk macosx10.9 \
    -configuration Debug \
    build
