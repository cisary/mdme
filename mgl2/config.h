#ifndef _MGL_CONFIG_H_
#define _MGL_CONFIG_H_

#define MGL_USE_DOUBLE 1
#define MGL_NO_DATA_A 0

#define MGL_HAVE_GSL 0
#define MGL_HAVE_LTDL 
#if defined(_MSC_VER) || defined(__BORLANDC__)
#define MGL_HAVE_PTHREAD 0
#else
#define MGL_HAVE_PTHREAD 1
#endif
#define MGL_HAVE_HDF4 0
#define MGL_HAVE_HDF5 0
#define MGL_HAVE_JPEG 0
#define MGL_HAVE_PNG 1
#define MGL_HAVE_ZLIB 1
#define MGL_HAVE_PDF 0
#define MGL_HAVE_GIF 0
#define MGL_HAVE_OPENGL 1
#define MGL_HAVE_MPI 0
#define MGL_FONT_PATH "/usr/local/share/mathgl/fonts"

#endif
